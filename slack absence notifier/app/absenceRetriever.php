<?php

  //Set timezome and get todays date
  date_default_timezone_set('UTC');
  $today = date("j");
  $todaysFullDate = date("l j F Y");

  $oakhouseFirstFloorEmployees = 'sophia.anele,christopher.boulter,david.cartlidge,brandon.conley,robert.culshaw,adam.davis,joana.esteves,lazaros.fantidis,rachel.gregory,chido.mabanda,parimalam.manoharam,chris.muir,marc.muller,shital.rai,chris.saridis,shruti.sharma,nick.shore,amy.stuart,michael.tsilikidis,arkadiusz.zontek,lucia.galatolo,jen.smith,amanda.macmaster,jamie.norman,jack.phillips,sarah.burgess,julie-ann.doorbar,mark.mcgrath,rob.butlin,ben.brunsdon,john.antrobus,dummy.edays';

  //Base64 encode names to use in url
  $encodedOakhouseNames = base64_encode($oakhouseFirstFloorEmployees);

  //Curl request for data from K8's
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_URL => "http://dev-k8-master.dev.redeye.com:32361/api/v1/absences?day=". $today. "&show_only=" .$encodedOakhouseNames,
  ));
  $response = curl_exec($curl);
  $decodedResponse = json_decode($response);

  if(!curl_exec($curl)){
    die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
  }
  curl_close($curl);

  //Loop through to get absence index of All, AM or PM and get their absences
  foreach ($decodedResponse as $index => $value) {
    displayAbsences($index,$value);
  }

  //Function to break down absence object
  function displayAbsences($index,$value) {
    global $allDayAbsences,$allDayIndex, $morningAbsences,$morningIndex, $afternoonAbsences, $afternoonIndex, $allDayDataAvailable, $morningDataAvailable, $afternoonDataAvailable;

    //All day absences
    if($index == "ALL" && !count((array)$value) == 0){
      $allDayIndex = "*All Day Absences*";

      foreach ($value as $allDayAbsenceType => $absenceData) {
        if($allDayAbsenceType == 'BUSINESS_TRIP'){
          $allDayAbsenceType = 'BUSINESS TRIP';
        }else if ($allDayAbsenceType == 'VOLUNTEER_LEAVE'){
          $allDayAbsenceType = 'VOLUNTEER LEAVE';
        }
        $allDayAbsences.= "*$allDayAbsenceType*\n";
        foreach ($absenceData as $indx => $employeeData) {
          $allDayFirstName = $employeeData->{"FirstName"};
          $allDayLastName = $employeeData->{"LastName"};
          $allDayAbsences.=$allDayFirstName." ".$allDayLastName."\n";
        }
      }
      $allDayDataAvailable = true;
    }

    //Morning Absences
    if($index == "AM" && !count((array)$value) == 0){
      $morningIndex = "*Morning Absences*";

      foreach ($value as $morningAbsenceType => $absenceData) {
        if($morningAbsenceType == 'BUSINESS_TRIP'){
          $morningAbsenceType = 'BUSINESS TRIP';
        }else if ($morningAbsenceType == 'VOLUNTEER_LEAVE'){
          $morningAbsenceType = 'VOLUNTEER LEAVE';
        }
        $morningAbsences.="*$morningAbsenceType*\n";
        foreach ($absenceData as $indx => $employeeData) {
          $morningFirstName = $employeeData->{"FirstName"};
          $morningLastName = $employeeData->{"LastName"};
          $morningAbsences.=$morningFirstName." ".$morningLastName."\n";
        }
      }
      $morningDataAvailable = true;
    }

    //Afternoon Absences
    if($index == "PM" && !count((array)$value) == 0){
      $afternoonIndex = "*Afternoon Absences*";

      foreach ($value as $afternoonAbsenceType => $absenceData) {
        if($afternoonAbsenceType == 'BUSINESS_TRIP'){
          $afternoonAbsenceType = 'BUSINESS TRIP';
        }else if ($afternoonAbsenceType == 'VOLUNTEER_LEAVE'){
          $afternoonAbsenceType = 'VOLUNTEER LEAVE';
        }
        $afternoonAbsences.= "*$afternoonAbsenceType*\n";
        foreach ($absenceData as $indx => $employeeData) {
          $afternoonFirstName = $employeeData->{"FirstName"};
          $afternoonLastName = $employeeData->{"LastName"};
          $afternoonAbsences.= $afternoonFirstName." ".$afternoonLastName."\n";
        }
      }
      $afternoonDataAvailable = true;
    }
  }
?>

